<?php
/**
 * Override or insert variables into the html template.
 */
function conk_process_html(&$vars) { 
  // Add classes for the font styles 
  $classes = explode(' ', $vars['classes']); 
  $classes[] = theme_get_setting('font_size');
  $vars['classes'] = trim(implode(' ', $classes));
}

