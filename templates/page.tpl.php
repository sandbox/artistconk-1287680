<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page.
 */
?>

  <div id="page">

    <?php print render($page['top']); ?>

    <header role="banner" class="clearfix">

      <?php if ($logo): ?>
        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
          <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
        </a>
      <?php endif; ?>

      <?php if ($site_name): ?>
        <h1 id="site-name">
		  <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
        </h1>
      <?php endif; ?>

      <?php if ($site_slogan): ?>
        <h2 id="site-slogan"><?php print $site_slogan; ?></h2>
      <?php endif; ?>

      <?php print render($page['header']); ?>

    </header>

    <nav role="navigation">
      <?php print render($page['menu_bar']); ?>
    </nav>

    <?php print render($page['preface']); ?>

    <?php if ($breadcrumb): ?>
      <nav id="breadcrumb"><?php print $breadcrumb; ?></nav>
    <?php endif; ?>

    <?php print $messages; ?>

    <div id="main-wrapper" class="clearfix">

      <div role="main">
      <?php
      if ($is_front): ?><div id="welcome_message">
        <?php print "Welcome to My Site!"; ?></div>
      <?php endif; ?>
        <a id="main-content"></a>
        <?php print render($title_prefix); ?>
        <?php if ($title): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
        <?php print render($title_suffix); ?>
        <?php if ($tabs): ?><div class="tabs"><?php print render($tabs); ?></div><?php endif; ?>
        <?php print render($page['help']); ?>
        <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
        <?php print render($page['content']); ?>
        <?php print $feed_icons; ?>
      </div> <!-- /#content -->

      <?php if ($page['sidebar_first']): ?>
        <div id="sidebar-first" class="column sidebar">
          <?php print render($page['sidebar_first']); ?>
        </div> <!-- /#sidebar-first -->
      <?php endif; ?>

      <?php if ($page['sidebar_second']): ?>
        <div id="sidebar-second" class="column sidebar">
          <?php print render($page['sidebar_second']); ?>
        </div> <!-- /#sidebar-second -->
      <?php endif; ?>

    </div></div> <!-- /#main, /#main-wrapper -->

	<?php print render($page['postscript']); ?>

  <?php if ($page['footer']): ?>
    <footer><?php print render($page['footer']); ?></footer>
  <?php endif; ?>

  </div> <!-- /#page -->

