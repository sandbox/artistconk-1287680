<?php 

function conk_form_system_theme_settings_alter(&$form, &$form_state) {

// Add some CSS so we can style our form in any theme.
  drupal_add_css(drupal_get_path('theme', 'conk') . '/css/conk.settings.form.css', array('group' => CSS_THEME));

// Layout settings
  $form['conk'] = array(
    '#type' => 'vertical_tabs',
    '#weight' => -10,
    '#default_tab' => 'defaults',
  );
/**
 * Font-size settings
 */
  $form['conk']['font'] = array( 
	'#type'          => 'fieldset', 
	'#title'         => t('Font size'), 
  );
  $form['conk']['font']['font_size'] = array( 
	'#type'          => 'select', 
	'#title'         => t('Font size'), 
	'#default_value' => theme_get_setting('font_size'), 
	'#description'   => t('Selecting a font size here affects relative font-size across the site.'), 
	'#options'       => array(
	  'fs-x-small'        => t('X-Small'), 
	  'fs-small'        => t('Small'), 
	  'fs-medium'        => t('Medium'), 
	  'fs-large'        => t('Large'), 
	  'fs-x-large'        => t('X-Large'), 
	  'fs-xx-large'        => t('XX-Large'), 
	  'fs-xxx-large'        => t('XXX-Large'),
	), 
  );

/**
 * Breadcrumb settings
 */
  $form['conk']['breadcrumb'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Breadcrumb'),
  );
  $form['conk']['breadcrumb']['breadcrumb_display'] = array(
    '#type'          => 'select',
    '#title'         => t('Display breadcrumb'),
    '#default_value' => theme_get_setting('breadcrumb_display'),
    '#options'       => array(
      'yes'          => t('Yes'),
      'no'           => t('No'),
    ),
  );
  $form['conk']['breadcrumb']['breadcrumb_separator'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Breadcrumb separator'),
    '#description'   => t('Text only. Dont forget to include spaces.'),
    '#default_value' => theme_get_setting('breadcrumb_separator'),
    '#size'          => 8,
    '#maxlength'     => 10,
  );
  $form['conk']['breadcrumb']['breadcrumb_home'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Show the homepage link in breadcrumbs'),
    '#default_value' => theme_get_setting('breadcrumb_home'),
  );
  $form['theme_settings']['#collapsible'] = TRUE;
  $form['theme_settings']['#collapsed'] = TRUE;
  $form['logo']['#collapsible'] = TRUE;
  $form['logo']['#collapsed'] = TRUE;
  $form['favicon']['#collapsible'] = TRUE;
  $form['favicon']['#collapsed'] = TRUE;
}

