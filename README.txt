Conk is a base theme made up of code and ideas from other prominent base themes
including Zen, Fusion, AdaptiveTheme (and Corolla), Boron, HTML5 Base, and
Omega. The goal of Conk is to incorporate flexible and useful configuration
options while maintaining a lean codebase that's easy to understand and
maintain.